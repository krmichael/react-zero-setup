const HTMLWebpackPlugin = require('html-webpack-plugin');
const HTMLWebpackPluginConfig = new HTMLWebpackPlugin({
  template: __dirname + '/app/index.html',
  filename: 'index.html',
  inject: 'body'
});

module.exports = {
  mode: 'development',
  entry: __dirname + '/app/index.js', //arquivo que serve para o ponto de entrada do babel
  module: { //dizendo ao webpack o que fazer com o codigo que ele encontrou
    rules: [
      {
        test: /\.js$/, //pegando todos os arquivos que terminam com .js
        exclude: /node_modules/, //excluindo a pasta node_modules
        loader: 'babel-loader' //dizendo ao loader qual transformação deve ser executada
      }
    ]
  },
  output: { //Objeto que define o nome do arquivo transformado e onde ele sera salvo
    filename: 'transformed.js', //nome do arquivo transformando
    path: __dirname + '/build' //onde o arquivo transformado sera salvo
  },
  plugins: [HTMLWebpackPluginConfig]
};
